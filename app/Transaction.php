<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use softDeletes;

    protected $fillable = [
        'uuid',
        'name',
        'email',
        'number',
        'adress',
        'transaction_total',
        'transaction_status',

     ];
 
        protected $hidden = [
 
     ];

     public function details() {
         return $this->hasMany(TransactionDetail::class,'transactions_id');
     }
}
