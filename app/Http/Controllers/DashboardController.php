<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(){
        $income = Transaction::where('transaction_status','SUCCESS')->sum('transaction_total');
        $sales = Transaction::count();
        $items= Transaction::orderBy('id','DESC')->take(5)->get();
        $pie =[
          'pending'=> Transaction::where('transaction_status','pending')->count(),
          'failed'=>  Transaction::where('transaction_status','failed')->count(),
            'success'=>Transaction::where('transaction_status','success')->count(),
        ];
        return view('pages.Dashboard')->with([
                'income'=>$income,
                'sales'=>$sales,
                'items'=>$items,
                'pie'=>$pie

            ]);
    }

}
